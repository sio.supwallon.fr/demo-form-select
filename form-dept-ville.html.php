<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="./vendor/twbs/bootstrap/dist/css/bootstrap.min.css">

        <title>Hello, world!</title>
    </head>
    <body>
        <form id="form_dept_ville" method="POST">

            <!-- Liste des Départements -->
            <div class="form-group row">
                <label for="departement" class="col-1 col-form-label">Département</label>
                <div class="col-5">
                    <select id="departement" name="id_departement" class="form-control">
                        <option>Choisir</option>
<?php foreach($departements as $value => $label): ?>
<?php if(isset($id_departement) && $id_departement == $value): ?>
                        <option value="<?= $value ?>" selected><?= $label ?></option>
<?php else: ?>
                        <option value="<?= $value ?>"><?= $label ?></option>
<?php endif; ?>
<?php endforeach; ?>
                    </select>
                </div>
            </div>

            <!-- Liste des Villes -->
<?php if(isset($villes)): ?>
    <div class="form-group row">
                <label for="ville" class="col-1 col-form-label">Ville</label>
                <div class="col-5">
                    <select id="ville" name="id_ville" class="form-control">
                        <option>Choisir</option>
<?php foreach($villes as $value => $label): ?>
<?php if(isset($id_departement) && $id_departement == $value): ?>
                        <option value="<?= $value ?>" selected><?= $label ?></option>
<?php else: ?>
                        <option value="<?= $value ?>"><?= $label ?></option>
<?php endif; ?>
<?php endforeach; ?>
                    </select>
                </div>
            </div>
<?php endif; ?>

        </form>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="./vendor/components/jquery/jquery.min.js"></script>
        <script src="./vendor/twbs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="./form-dept-ville.js"></script>
    </body>
</html>
