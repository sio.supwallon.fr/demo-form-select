<?php

$departements = [
    59 => "Nord",
    62 => "Pas-de-Calais",
];

$table_villes = [
    59 => [
        1 => "Dunkerque",
        2 => "Lille",
        3 => "Valenciennes",
    ],
    62 => [
        1 => "Arras",
        2 => "Béthune",
        3 => "Calais",
    ],
];

if(isset($_POST['id_departement'])) {
    $id_departement = filter_input(INPUT_POST, 'id_departement', FILTER_VALIDATE_INT);
    $villes = $table_villes[$id_departement];
}

require_once "form-dept-ville.html.php";